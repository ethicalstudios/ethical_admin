<?php
/**
 * @file
 * ethical_admin.pm_existing_pages.inc
 */

/**
 * Implements hook_pm_existing_pages_info().
 */
function ethical_admin_pm_existing_pages_info() {
  $export = array();

  $pm_existing_page = new stdClass();
  $pm_existing_page->api_version = 1;
  $pm_existing_page->name = 'user_profile_edit';
  $pm_existing_page->label = 'User profile edit';
  $pm_existing_page->context = 'entity|user|uid';
  $pm_existing_page->paths = '/user/%user/edit';
  $export['user_profile_edit'] = $pm_existing_page;

  return $export;
}
