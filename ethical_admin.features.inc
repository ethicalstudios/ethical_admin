<?php
/**
 * @file
 * ethical_admin.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ethical_admin_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "pm_existing_pages" && $api == "pm_existing_pages") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_page_manager_handlers_alter().
 */
function ethical_admin_default_page_manager_handlers_alter(&$data) {
  if (isset($data['node_edit_panel_context'])) {
    $data['node_edit_panel_context']->conf['display']->content['new-51126c95-cf51-4ad6-a27a-11e4260d8023']->configuration['override_title_text'] = 'Category'; /* WAS: 'Content category' */
    $data['node_edit_panel_context']->conf['display']->content['new-51126c95-cf51-4ad6-a27a-11e4260d8023']->panel = 'contentmain'; /* WAS: 'sidebar' */
    $data['node_edit_panel_context']->conf['display']->content['new-51126c95-cf51-4ad6-a27a-11e4260d8023']->position = 4; /* WAS: 1 */
    $data['node_edit_panel_context']->conf['display']->content['new-6e4a98d8-3738-41b9-8c33-4b4c905c6834']->position = 3; /* WAS: 5 */
    $data['node_edit_panel_context']->conf['display']->content['new-99f6c0bd-36fd-411b-9833-c0be0fab7548']->position = 1; /* WAS: 2 */
    $data['node_edit_panel_context']->conf['display']->content['new-acdc066a-b785-4baa-99ad-eb8fe70429fb'] = (object) array(
          'pid' => 'new-acdc066a-b785-4baa-99ad-eb8fe70429fb',
          'panel' => 'contentmain',
          'type' => 'entity_form_field',
          'subtype' => 'node:field_project',
          'shown' => TRUE,
          'access' => array(),
          'configuration' => array(
            'label' => '',
            'formatter' => '',
            'context' => 'argument_node_edit_1',
            'override_title' => 1,
            'override_title_text' => 'Project',
          ),
          'cache' => array(),
          'style' => '',
          'css' => array(),
          'extras' => array(),
          'position' => 3,
          'locks' => array(),
          'uuid' => 'acdc066a-b785-4baa-99ad-eb8fe70429fb',
        ); /* WAS: '' */
    $data['node_edit_panel_context']->conf['display']->content['new-c0926cca-059c-4bb1-bccd-2838ec7a93f2']->position = 4; /* WAS: 6 */
    $data['node_edit_panel_context']->conf['display']->content['new-cb6bcf52-b51c-4e7e-a348-84d4081d8273'] = (object) array(
          'pid' => 'new-cb6bcf52-b51c-4e7e-a348-84d4081d8273',
          'panel' => 'contentmain',
          'type' => 'entity_form_field',
          'subtype' => 'node:field_country',
          'shown' => TRUE,
          'access' => array(),
          'configuration' => array(
            'label' => '',
            'formatter' => '',
            'context' => 'argument_node_edit_1',
            'override_title' => 1,
            'override_title_text' => 'Country',
          ),
          'cache' => array(),
          'style' => '',
          'css' => array(),
          'extras' => array(),
          'position' => 5,
          'locks' => array(),
          'uuid' => 'cb6bcf52-b51c-4e7e-a348-84d4081d8273',
        ); /* WAS: '' */
    $data['node_edit_panel_context']->conf['display']->content['new-e38ab3a3-24d0-4ac2-900e-4ddb0248be0c']->position = 2; /* WAS: 3 */
    $data['node_edit_panel_context']->conf['display']->content['new-fc76a7e2-ac1b-4ed7-b79b-3ee8c28c38c6'] = (object) array(
          'pid' => 'new-fc76a7e2-ac1b-4ed7-b79b-3ee8c28c38c6',
          'panel' => 'contentmain',
          'type' => 'entity_form_field',
          'subtype' => 'node:field_group',
          'shown' => TRUE,
          'access' => array(),
          'configuration' => array(
            'label' => '',
            'formatter' => '',
            'context' => 'argument_node_edit_1',
            'override_title' => 1,
            'override_title_text' => 'Group',
          ),
          'cache' => array(),
          'style' => '',
          'css' => array(),
          'extras' => array(),
          'position' => 6,
          'locks' => array(),
          'uuid' => 'fc76a7e2-ac1b-4ed7-b79b-3ee8c28c38c6',
        ); /* WAS: '' */
    $data['node_edit_panel_context']->conf['display']->layout = 'oe-admin'; /* WAS: 'radix_burr_flipped' */
    $data['node_edit_panel_context']->conf['display']->panels['contentmain'][3] = 'new-51126c95-cf51-4ad6-a27a-11e4260d8023'; /* WAS: '' */
    $data['node_edit_panel_context']->conf['display']->panels['contentmain'][4] = 'new-acdc066a-b785-4baa-99ad-eb8fe70429fb'; /* WAS: '' */
    $data['node_edit_panel_context']->conf['display']->panels['contentmain'][5] = 'new-cb6bcf52-b51c-4e7e-a348-84d4081d8273'; /* WAS: '' */
    $data['node_edit_panel_context']->conf['display']->panels['contentmain'][6] = 'new-fc76a7e2-ac1b-4ed7-b79b-3ee8c28c38c6'; /* WAS: '' */
    $data['node_edit_panel_context']->conf['display']->panels['sidebar'][1] = 'new-99f6c0bd-36fd-411b-9833-c0be0fab7548'; /* WAS: 'new-51126c95-cf51-4ad6-a27a-11e4260d8023' */
    $data['node_edit_panel_context']->conf['display']->panels['sidebar'][2] = 'new-e38ab3a3-24d0-4ac2-900e-4ddb0248be0c'; /* WAS: 'new-99f6c0bd-36fd-411b-9833-c0be0fab7548' */
    $data['node_edit_panel_context']->conf['display']->panels['sidebar'][3] = 'new-6e4a98d8-3738-41b9-8c33-4b4c905c6834'; /* WAS: 'new-e38ab3a3-24d0-4ac2-900e-4ddb0248be0c' */
    $data['node_edit_panel_context']->conf['display']->panels['sidebar'][4] = 'new-c0926cca-059c-4bb1-bccd-2838ec7a93f2'; /* WAS: 'new-9176b962-e64f-44a6-8ebe-e03ebf0f6c3d' */
    unset($data['node_edit_panel_context']->conf['display']->content['new-9176b962-e64f-44a6-8ebe-e03ebf0f6c3d']);
    unset($data['node_edit_panel_context']->conf['display']->panels['sidebar'][5]);
    unset($data['node_edit_panel_context']->conf['display']->panels['sidebar'][6]);
  }
  if (isset($data['user_edit_panel_context'])) {
    $data['user_edit_panel_context']->conf['display']->panel_settings['style_settings']['footer'] = NULL; /* WAS: '' */
    unset($data['user_edit_panel_context']->conf['display']->content['new-be0c59f6-016f-4a3d-a888-dd3cf534796f']);
    unset($data['user_edit_panel_context']->conf['display']->panels['sidebar']);
  }
}
