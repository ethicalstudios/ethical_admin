(function ($) {

  Drupal.behaviors.EthicalAdmin = {
    attach: function (context, settings) {
      if($('div.panels-ipe-editing').length){

        $('div.panels-ipe-portlet-content').each(function(){
          if(!$(this).children('div.panel-pane, div.wide-panel').length){
            if($.trim($(this).html()) !== ''){
              $(this).wrapInner('<div class="pane-body"></div>');
            }
            else{
              //if empty it's probably a mini pane with nothing in it. Hide from editors
              $(this).parent().hide();
            }

          }
        });

        //hide the panel delete, edit and style buttons for panels that editors cannot re-add if they accidentally delete
        $('div.panels-ipe-portlet-content>div.pane-body, div.panels-ipe-portlet-content>div.pane-main-menu, div.panels-ipe-portlet-content>div.field-featured-image, div.panels-ipe-portlet-content>div.pane-disqus-disqus-comments, , div.panels-ipe-portlet-content>p.address').each(function(){
          $(this).closest('.panels-ipe-portlet-wrapper').find('ul.panels-ipe-linkbar').hide();
        });

        //if($('.panels-ipe-empty-pane').length){

          $('.panels-ipe-empty-pane').each(function(){

            var content = $.trim($(this).find('div.panel-pane').html());

            //hide some empty panels from editors ... but not all otherwise they won't be able to see if they've put, for example, a list that is empty
            if(content.indexOf('"This node"') >= 0 ||
              content.indexOf('"Disqus: Comments"') >= 0 ||
              content.indexOf('"Main menu (levels 2+)"') >= 0){

              $(this).parent().hide();
            }
          });
        //}

        //if($('.panels-ipe-nodrag').length){
          $('.panels-ipe-nodrag .delete').hide();
        //}
        //


      }
    }
  };

})(jQuery);

